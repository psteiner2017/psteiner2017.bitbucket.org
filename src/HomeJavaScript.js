var mLoop = setInterval(MainLoop, 35);

var slide = document.getElementsByClassName('slide'); // Get Slides
for (var i = 0; i < slide.length; i++) {if(i != 0) {slide[i].style.visibility = "hidden";}}
var slideshow = document.getElementById("Slideshow");

var ExpansionButton = document.getElementById('ExpansionButton'); // Get Expansion Button
ExpansionButton.addEventListener("click", ExpansionClick);

var SlideContent = document.getElementById('SlideContent');
var HeaderBox = document.getElementsByClassName('HeaderBox');
var ContentBox = document.getElementsByClassName('ContentBox');

var initialTextDiv = document.getElementById('initialTextDiv');

/*
    ++ Content Variables ++
*/

var UpperHeaderPos = 5;
var MiddleHeaderPos = 20;

var UpperContentPos = 40;


// -----------------------------------

var HeaderPos = 0; // Top: ...
var TargetHeaderPos = MiddleHeaderPos;
var HeaderSpeed = 8.000;

var HeaderOpac = 0;
var TargetHeaderOpac = 1;
var ActiveOpacHeader = 0;

var ContentPos = 100; // Top ...
var TargetContentPos = 100;
var ContentSpeed = 6.000;


var ContentSnapTol = 0.25;

var OpacSpeed = 0.06;
var OpacSnapTol = 0.08;


// -----------------------------------


var MouseX, MouseY;
var keyPressed;
var keyBounce = 0;

var CurrentSlide = 0;
var PrevSlide = 3;
var SlideTransDir;
var PageState = 0; // 0 = Slide Stable, 1 = Slide Moving, 2 = Text Pop Out Moving, 3 = Text Pop Out Stable

var ExpansionDir = 0; // -1 = Closing, 1 = Opening
var brightAmount = 0;
var brightTarget = 0;

var brightSpeed = 1.5; // 1.25
var brightSnapTol = 2;

var snapTol = 2;
var MoveSpeed = 6.00;

// Expansion Button rotation variables

var ExpRotPos = 180;
var TargetExpRotPos = 180;
var ExpRotSpeed = 2.00;
var ExpButSnapTol = 1;

// Initial Text fade out vars

var initTxtOpac = 1.000;
var initTxtOpacTar = 1.000;
var initTxtFadeSpd = 0.06;
var initTxtOpacTol = 0.04;

// ----------------------------------------------
// Mobile Support Dev
// ----------------------------------------------

var hammertime = new Hammer(slideshow); // Include Hammertime Library

hammertime.get('swipe').set({ direction: Hammer.DIRECTION_ALL });

hammertime.on("swipeleft", function(ev) {
	ChangeSlide(1);
});
hammertime.on("swiperight", function(ev) {
	ChangeSlide(-1);
});
hammertime.on("swipeup swipedown", function(ev) {
	ExpansionClick();
});

// ----------------------------------------------

document.addEventListener("mousemove", mousePos);
function mousePos() {
    MouseX = event.clientX;
    MouseY = event.clientY;
}

document.addEventListener("keydown", SlideKeyShift);
function SlideKeyShift() {
    keyPressed = event.keyCode;
    if(keyPressed == 39 && keyBounce == 0) { // Right Arrow
        keyBounce = 1;
        ChangeSlide(1);
    }
    else if(keyPressed == 37 && keyBounce == 0) { // Left Arrow
        keyBounce = 1;
        ChangeSlide(-1);
    }
    else if(keyPressed == 38 && keyBounce == 0) { // Up Arrow
        keyBounce = 1;
        if (PageState == 0) {
            ExpansionClick();
        }
    }
    else if(keyPressed == 40 && keyBounce == 0) { // Down Arrow
        keyBounce = 1;
        if (PageState == 3) {
            ExpansionClick();
        }
    }
}
document.addEventListener("keyup", keyB);
function keyB() {
    keyBounce = 0;
}
slideshow.addEventListener("click", mClick);
function mClick() {
    ChangeSlide(1);
}

function ExpansionClick() {
    switch (PageState) {
        case 0:
            PageState = 2;
            ExpansionDir = 1;
            brightTarget = 18; // Darken 20 units
            TargetContentPos = UpperContentPos;
            TargetHeaderPos = UpperHeaderPos;
            break;
        case 3:
            PageState = 2;
            ExpansionDir = -1;
            brightTarget = 0; // Reset Blur
            TargetContentPos = 100;
            TargetHeaderPos = MiddleHeaderPos;
            break;
    }
}


function MainLoop() {
    // console.log("CSlide:");
    // console.log(CurrentSlide);
    // console.log("PSlide");
    // console.log(PrevSlide);
    switch (PageState) {
        case 0: // Slide Stable
            TargetHeaderOpac = 1; // Fade in Header
            ActiveOpacHeader = CurrentSlide; // Fade in Current Header

            slide[CurrentSlide].style.visibility = "visible";
        break;
        case 1: // Slide Moving
            AnimateSlideTransition(SlideTransDir);
            if (CurrentSlide == 0) {initTxtOpacTar = 1.000; initTxtFadeSpd = 0.02;}
            else {initTxtOpacTar = 0.000; initTxtFadeSpd = 0.06;}
        break;
        case 2: // Text Pop Out Moving
            AnimateContentExpansion(ExpansionDir);
            initTxtOpacTar = 0;
            initTxtFadeSpd = 0.06;
        break;
        case 3: // Text Pop Out Stable
            // for (var i = 0; i < slide.length; i++) { // Cycle through slides
            //     slide[i].style.visibility = "hidden"; // Set All slides to hidden
            // }
            slide[CurrentSlide].style.visibility = "visible";
        break;
    }
    MoveContentsToTarget();
    HeaderOpacityToTarget();
    AnimateButtonRotation();
    AnimateInitialText();
}



function ChangeSlide(LR) {
    if (PageState == 0) {
        PrevSlide = CurrentSlide;
        if (LR > 0 && CurrentSlide < slide.length-1) { // Pan Left
            CurrentSlide += LR;
            SlideTransDir = 1;

            midline = window.innerWidth;
            midlineTarget = 0;
        }
        else if (LR > 0 && CurrentSlide == slide.length-1) { // Pan Left
            CurrentSlide = 0;
            SlideTransDir = 1;

            midline = window.innerWidth;
            midlineTarget = 0;
        }
        else if (LR < 0 && CurrentSlide > 0) { // Pan Right
            CurrentSlide += LR;
            SlideTransDir = -1;

            midline = 0;
            midlineTarget = window.innerWidth;
        }
        else if (LR < 0 && CurrentSlide == 0) { // Pan Right
            CurrentSlide = slide.length-1;
            SlideTransDir = -1;

            midline = 0;
            midlineTarget = window.innerWidth;
        }
        PageState = 1;
        TargetHeaderOpac = 0; // Fade out header
        ActiveOpacHeader = PrevSlide; // Fade out old header
        
        for (var i = 0; i < ContentBox.length; i++) {
            if (i!=CurrentSlide) {ContentBox[i].style.visibility = "hidden";}
        }
        ContentBox[CurrentSlide].style.visibility = "visible";
    }
}


var midline;
var midlineTarget;
function AnimateSlideTransition(LR) {

    for (var i = 0; i < slide.length; i++) { // Cycle through slides
        if (i != CurrentSlide && i != PrevSlide) {slide[i].style.visibility = "hidden";} // Set All slides but current and previous slides to hidden
    }

    slide[CurrentSlide].style.visibility = "visible"; // Set used slides to visable
    slide[PrevSlide].style.visibility = "visible";

    switch (LR) {
        case -1: // Pan Right
            slide[CurrentSlide].style.right = ""; // Remove Values from right
            slide[PrevSlide].style.right = "";

            midline += (midlineTarget - midline)/MoveSpeed;

            slide[CurrentSlide].style.left = String(midline - window.innerWidth) + "px";
            slide[PrevSlide].style.left = String(midline) + "px";

            if (midline >= window.innerWidth - snapTol) {
                midline = midlineTarget;

                slide[CurrentSlide].style.left = String(midline - window.innerWidth) + "px";
                slide[PrevSlide].style.left = String(midline) + "px";
    
                slide[PrevSlide].style.visibility = "hidden";
                PageState = 0;

                HeaderPos = 0; // Bring in header from top
                TargetHeaderPos = MiddleHeaderPos;
            }
        break;
        case 1: // Pan Left
            slide[CurrentSlide].style.left = ""; // Remove Values from left
            slide[PrevSlide].style.left = "";

            
            midline += (midlineTarget - midline)/MoveSpeed;

            slide[CurrentSlide].style.right = String(-1 * midline) + "px";
            slide[PrevSlide].style.right = String((-1 * midline) + window.innerWidth) + "px";

            if (midline <= snapTol) {
                midline = midlineTarget;

                slide[CurrentSlide].style.right = String(-1 * midline) + "px";
                slide[PrevSlide].style.right = String((-1 * midline) + window.innerWidth) + "px";

                slide[PrevSlide].style.visibility = "hidden";
                PageState = 0;

                HeaderPos = 0; // Bring in header from top
                TargetHeaderPos = MiddleHeaderPos;
            }
        break;
    }
}

function AnimateContentExpansion(ExpDir) { // -1 for closing, 1 for opening
    switch (ExpDir) {
        case -1:
            if (brightAmount > brightTarget) {
                brightAmount -= brightSpeed;
                // slide[CurrentSlide].style.webkitFilter = ("blur(" + String(brightAmount) + "px)");
                if (brightAmount < brightTarget+brightSnapTol) {
                    brightAmount = brightTarget;
                    PageState = 0;
                    slide[CurrentSlide].style.webkitFilter = "";
                    TargetExpRotPos = 180; // Rotate back to normal
                }
            }
            
            break;
        case 1:
            // slide[CurrentSlide].style.webkitFilter = "";
            if (brightAmount < brightTarget) {
                brightAmount += brightSpeed;
                if (brightAmount > (brightTarget-brightSnapTol)) {
                    brightAmount = brightTarget;
                    PageState = 3;
                    TargetExpRotPos = 0; // Rotate button around
                }
                // slide[CurrentSlide].style.webkitFilter = ("blur(" + String(brightAmount) + "px)");
            }
            
            break;
    }
    slide[CurrentSlide].style.webkitFilter = ("brightness(" + String(100-(brightAmount*2.5)) + "%)");
}

function MoveContentsToTarget() {
    if (HeaderPos != TargetHeaderPos) { // Animate HeaderBox to its target
        HeaderPos += (TargetHeaderPos - HeaderPos) / HeaderSpeed;
        if (HeaderPos > (TargetHeaderPos - ContentSnapTol) && HeaderPos < (TargetHeaderPos + ContentSnapTol)) {
            HeaderPos = TargetHeaderPos;
        }
        HeaderBox[CurrentSlide].style.top = String(HeaderPos) + "%";
    }

    if (ContentPos != TargetContentPos) {
        ContentPos += (TargetContentPos - ContentPos) / ContentSpeed;
        if (ContentPos > (TargetContentPos - ContentSnapTol) && ContentPos < (TargetContentPos + ContentSnapTol)) {
            ContentPos = TargetContentPos;
        }
        ContentBox[CurrentSlide].style.top = String(ContentPos) + "%";
    }
}

function HeaderOpacityToTarget() {
    if (HeaderOpac != TargetHeaderOpac) {
        if (HeaderOpac < TargetHeaderOpac) {
            HeaderOpac += OpacSpeed;
            if (HeaderOpac > TargetHeaderOpac - OpacSnapTol) {
                HeaderOpac = TargetHeaderOpac;
            }
        }
        else {
            HeaderOpac -= OpacSpeed;
            if (HeaderOpac < TargetHeaderOpac + OpacSnapTol) {
                HeaderOpac = TargetHeaderOpac;
            }
        }
        HeaderBox[ActiveOpacHeader].style.opacity = String(HeaderOpac);
    }
    // console.log(ActiveOpacHeader);
}

function AnimateButtonRotation() {
    if (ExpRotPos != TargetExpRotPos) {
        ExpRotPos += (TargetExpRotPos - ExpRotPos) / ExpRotSpeed;
        if (ExpRotPos > TargetExpRotPos - ExpButSnapTol && ExpRotPos < TargetExpRotPos + ExpButSnapTol) {
            ExpRotPos = TargetExpRotPos;
        }
        ExpansionButton.style.transform = "rotate(" + String(ExpRotPos) + "deg)";
        ExpansionButton.style.webkitTransform = "rotate(" + String(ExpRotPos) + "deg)";
    }
}

function AnimateInitialText() {
    if (initTxtOpac != initTxtOpacTar) {
        if (initTxtOpac > initTxtOpacTar) {
            initTxtOpac -= initTxtFadeSpd;
            if (initTxtOpac < initTxtOpacTar + initTxtOpacTol) {
                initTxtOpac = initTxtOpacTar;
            }
        }
        else {
            initTxtOpac += initTxtFadeSpd;
            if (initTxtOpac > initTxtOpacTar - initTxtOpacTol) {
                initTxtOpac = initTxtOpacTar;
            }
        }
        console.log(initTxtOpac);
        initialTextDiv.style.opacity = String(initTxtOpac);
        if (initTxtOpac > 0) {
            initialTextDiv.style.visibility = "visible";
        }
        else {
            initialTextDiv.style.visibility = "hidden";
        }
    }
}